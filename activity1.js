// 1
db.users.insertMany([
	{	
		"firstName": "Diane",
		"lastName": "Murphy",
		"email": "dmurphy@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{	
		"firstName": "Mary",
		"lastName": "Patterson",
		"email": "mpatterson@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{	
		"firstName": "Jeff",
		"lastName": "Firrelli",
		"email": "jfirrelli@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{	
		"firstName": "Gerard",
		"lastName": "Bondur",
		"email": "gbondur@mail.com",
		"isAdmin": false,
		"isActive": true
	},
	{	
		"firstName": "Pamela",
		"lastName": "Castillo",
		"email": "pcastillo@mail.com",
		"isAdmin": true,
		"isActive": false
	},
	{	
		"firstName": "George",
		"lastName": "Vanauf",
		"email": "gvanauf@mail.com",
		"isAdmin": true,
		"isActive": true
	}
]);

// 2
db.courses.insertMany([
	{	
		"name": "Professional Development",
		"price": 10000.0
	},
	{	
		"name": "Business Processing",
		"price": 13000.0
	}
]);

// 3
db.users.find(
	{"isAdmin": false}
);

// 4
db.courses.updateOne(
	{"name": "Professional Development"},
	{
		$set: {
			"enrollees": [
				db.users.find({"isAdmin": false})[0]["_id"],
				db.users.find({"isAdmin": false})[1]["_id"]
			]
		}
	}
);